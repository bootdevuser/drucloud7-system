Description
===========

This module enables the website to permanently store the access tokens
that the module fboauth retrieves from Facebook when the user grants access
to the application that fboauth is managing.

Usage
=====

Once the module is enabled, each user will have a page at /user/UID/fboauth
that allows the user to grant the application all permissions required for all
actions declared in the hook_fboauth_actions implementations.

This allows any module that uses fboauth to use the stored token to perform
calls to facebook's api without having to redirect the user to FB (which is the
behaviour fboauth uses), or even without having any interaction from the user
(ie: from hook_cron).

This is useful when a module wants to perform a call to Facebook on behalf of
a user but it is not possible to use the normal workflow of redirecting the
user to facebook to get a valid access_token.

Modules can implement calls to facebook api this way:

<?php

// Retrieve stored access_token for the current user.
$access_token = fboauth_tokens_get_user_access_token();

// Call facebook api.
$params = array(
  'message' => 'Hello world',
);
$result = fboauth_graph_query('me/feed', $access_token, $params, 'POST');

?>

The module does not provide any facility to know if the provided token is still
valid.

So if your call to fboauth_graph_query() returns an error, your module will be
responsible to hint the user to go to /user/UID/fboauth to get a new access
token.

I have also published a module called fboauth_publish which uses this module and
allows the user to post nodes to Facebook walls from the node form and doesn't
redirects the user to FB for getting an access token.
