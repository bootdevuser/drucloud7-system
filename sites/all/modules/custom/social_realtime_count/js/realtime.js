(function ($) {
    Drupal.behaviors.social_realtime_counter = {
        attach: function(context) {
        	var content_url = Drupal.settings.r_link.urls;

		    realtime_social(); //run function
		    setInterval(realtime_social, 30000); //set-time 30secs

		  function realtime_social() {
		  	var fburl = '//graph.facebook.com/'+ content_url;
		  	var twurl = '//urls.api.twitter.com/1/urls/count.json?url='+ content_url + '&callback=?';
			    $.getJSON(fburl, function(data) {

			      var shares1 = parseInt(data.shares);
			       if(isNaN(shares1)){
			       	  shares1 = 0;
			       }

			      $('.fb-count').text(nFormatter(shares1));
			      
				      $.getJSON(twurl,function(data) {
				      var shares = parseInt(data.count);
				      $('.tw-count').text(nFormatter(shares));
				      var sum1 = shares1 + shares;
				      var sum1 = nFormatter(parseInt(sum1));  
				    });
			   });
		    }
            //comma
			function addCommas(nStr) {
			    nStr += '';
			    x = nStr.split('.');
			    x1 = x[0];
			    x2 = x.length > 1 ? '.' + x[1] : '';
			    var rgx = /(\d+)(\d{3})/;
			    while (rgx.test(x1)) {
			      x1 = x1.replace(rgx, '$1' + ',' + '$2');
			    }
			    return x1 + x2;
			}
            //convert and add suffix with 1k,1M and 1G
			function nFormatter(num) {
			    if (num >= 1000000000) {
			        return (num / 1000000000).toFixed(1) + 'G';
			    }
			    if (num >= 1000000) {
			        return (num / 1000000).toFixed(1) + 'M';
			    }
			    if (num >= 1000) {
			        return (num / 1000).toFixed(1) + 'K';
			    }
			    return num;
			}
        }
    }
})(jQuery);