<?php
/**
 * @file
 * drucloud_photo_essay.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function drucloud_photo_essay_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'cn_photo_essay';
  $context->description = 'drucloudPhoto Essay page';
  $context->tag = 'drucloudPhoto Essay';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'photo_essay' => 'photo_essay',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-0ac080f126aecbc3e500d2928622b664' => array(
          'module' => 'views',
          'delta' => '0ac080f126aecbc3e500d2928622b664',
          'region' => 'content_bottom',
          'weight' => '-10',
        ),
        'drucloud_base_content-drucloud_prev_next' => array(
          'module' => 'drucloud_base_content',
          'delta' => 'drucloud_prev_next',
          'region' => 'content_bottom',
          'weight' => '-9',
        ),
        'views-3ad264007cceb918c3198c0d8e75e09d' => array(
          'module' => 'views',
          'delta' => '3ad264007cceb918c3198c0d8e75e09d',
          'region' => 'content_bottom',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('drucloudPhoto Essay');
  t('drucloudPhoto Essay page');
  $export['cn_photo_essay'] = $context;

  return $export;
}
