<?php
/**
 * @file
 * drucloud_users.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function drucloud_users_taxonomy_default_vocabularies() {
  return array(
    'staff_affiliation' => array(
      'name' => 'Staff affiliation',
      'machine_name' => 'staff_affiliation',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
