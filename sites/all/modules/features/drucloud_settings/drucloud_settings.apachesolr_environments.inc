<?php
/**
 * @file
 * drucloud_settings.apachesolr_environments.inc
 */

/**
 * Implements hook_apachesolr_environments().
 */
function drucloud_settings_apachesolr_environments() {
  $export = array();

  $environment = new stdClass();
  $environment->api_version = 1;
  $environment->env_id = 'solr';
  $environment->name = 'localhost server';
  $environment->url = 'http://localhost:8983/solr';
  $environment->service_class = '';
  $environment->conf = array(
    'apachesolr_direct_commit' => 0,
    'apachesolr_read_only' => '0',
    'apachesolr_soft_commit' => 0,
  );
  $environment->index_bundles = array(
    'node' => array(
      0 => 'article',
      1 => 'page',
    ),
  );
  $export['solr'] = $environment;

  return $export;
}
